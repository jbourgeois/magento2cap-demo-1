# Walkthrough
## Install local Hypernode Vagrant
    # Clone Hypernode-Vagrant repo in a separate project folder. 
    git clone git@github.com:ByteInternet/hypernode-vagrant.git <my-capistrano-project>
    # Move into that folder.
    cd <my-capistrano-project>
      
    # Execute `vagrant up` and follow the wizards.
    vagrant up
      
    # Consider adding extra aliasses to the `local.yml` file before running `vagrant up` for the seconde time.
    ##
    # hostmanager:
    #   extra-aliases:
    #   - my-capistrano-project.dev
    ##
    
## Install Magento2-Capistrano
    # Log in to the vagrant box using the `vagrant` user.
    vagrant ssh
    # Install the correct Ruby version.
    sudo apt-get update
    sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev
      
    cd /tmp
    git clone https://github.com/rbenv/rbenv.git ~/.rbenv
    echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
    echo 'eval "$(rbenv init -)"' >> ~/.bashrc
    exec $SHELL
      
    git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
    echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
    exec $SHELL
      
    rbenv install 2.3.1
    rbenv global 2.3.1
    ruby -v
      
    # `capistrano-magento2` and its dependencies.
    sudo gem install capistrano-magento2
    # Leave the Hypdernode-Vagrant ssh session
    exit

## Composer install Magento 2
    # Forward your local ssh key to future ssh sessions. (Bitbucket clone permissions.)
    ssh-add ~/.ssh/id_rsa
    
    # Log into your Hypernode-Vagrant instance
    ssh app@my-capistrano-project.dev
    
    # Add cap installation location to the app users executables path.
    echo 'export PATH=/home/vagrant/.rbenv/shims/:$PATH' >> ~/.bashrc 
    source ~/.bashrc
      
    # Remove default Hypernode files
    rm -rf ~/magento2/pub
      
    ########
    # TODO: Move local ~/.composer/auth.json to /data/web/.composer/auth.json 
    ########
      
    # Install Magento 2 Composer style
    composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition ~/magento2
    
    OR
    
    # Clone existing Magento 2 repository
    cd ~/magento2
    git clone git@bitbucket.org:indiegroup/demeesterwim-shop.git .
    
    
## Capistrano Setup and Configuration
    # On local Vagrant Hypernode
    cd ~/magento2
    mkdir -p tools/cap
    cd ./tools/cap
    cap install
    # TODO: configure capistrano

## Hypernode
    # Open an SSH session to your xxx.hypernode.io instance
    ssh app@xxx.hypernode.io
    # Make sure it is marked as a Magento 2 instance.
    touch ~/nginx/magento2.flag
     
    # Create the basic capistrano folder structure
    mkdir -p ~/capistrano/shared/app/etc/
     
    # Create hypernode related symlinks to capistrano folders.
    # (Don't worry the destinations don't exist yet, they soon will)
    # ==> Link '~/capistrano/current' to '~/magento2'
    rm -rf ~/magento2 && ln -s ~/capistrano/current ~/magento2
    # ==> Link '~/capistrano/current/pub' to '~/public'
    rm -rf ~/public && ln -s ~/capistrano/current/pub ~/public
     
    # Add the environment sensitive settings file. 
    # (You probably want to set 'MAGE_MODE' to 'production'.)
    nano shared/app/etc/env.php
     
    # Create DB & upload / install?
    # TODO...
     
    # Configure composer key,etc.
    # (TODO: document further [Magento keys + bitbucket + github] / [upload local file??])
    nano ~/.composer/auth.json 
     
    # TODO: Capstrano nginx config tweaks?
    http://davidalger.com/development/magento/deploying-magento-2-using-capistrano/

## Initial deploy
    # Execute on local Hypernode Vagrant 
    cd ~/magento2/tools/cap
    cap production deploy
    
# Notes
## References 
- https://github.com/davidalger/capistrano-magento2
- http://davidalger.com/development/magento/deploying-magento-2-using-capistrano/


## Various
- Installing 'capistrano-magento2' on OS X (Sierra)  
`sudo gem install -n /usr/local/bin capistrano-magento2`